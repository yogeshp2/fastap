from typing import Union ,List ,Optional

from fastapi import FastAPI
from pydantic import BaseModel, EmailStr

app = FastAPI()
#main aim : it Will limit the output data to that of the mode

class Item(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: list[str] = []


@app.post("/items/", response_model=Item) #<-- declear in path its Not of path operation function, like all the parameters and body. its decorator
async def create_item(item: Item):
    return item

                    # Return the same input data

class UserIn(BaseModel):
    username: str
    password: str
    email: EmailStr
    full_name: str
@app.post("/responss/",response_model=UserIn)
async def create_ser(user:UserIn):
    return user

class UserIn1(BaseModel):
    username: str
    password: str
    email: EmailStr
    full_name: Union[str, None] = None


# Don't do this in production!
@app.post("/user/", response_model=UserIn1)
async def create_user(user: UserIn1):
    return user

                    # add an output model
class UserIn2(BaseModel):
    username: str
    password: str  #<--
    email: EmailStr
    full_name: Union[str, None] = None


class UserOut(BaseModel):
    username: str
    email: EmailStr
    full_name: Union[str, None] = None


@app.post("/user1/", response_model=UserOut) #<-- password will not show here doesn't include the password
async def create_user(user: UserIn2):
    return user

                    # Response Model encoding parameters
class Item1(BaseModel):
    name: str
    description: Union[str, None] = None #
    price: float
    tax: float = 10.5
    tags: List[str] = [] #


items = {
    "foo": {"name": "Foo", "price": 50.2},
    "bar": {"name": "Bar", "description": "The bartenders", "price": 62, "tax": 20.2},
    "baz": {"name": "Baz", "description": None, "price": 50.2, "tax": 10.5, "tags": []},

}
# m = Item1(foo=1, bar=2, baz=3)
# optional data see na karvana ho then
@app.get("/items/{item_id}", response_model=Item1, response_model_exclude_unset=True) #<-Use the response_model_exclude_unset paramete
async def read_item(item_id: str):
    return items[item_id]


items = {
    "foo": {"name": "Foo", "price": 50.2},
    "bar": {"name": "Bar", "description": "The Bar fighters", "price": 62, "tax": 20.2},
    "baz": {
        "name": "Baz",
        "description": "There goes my baz",
        "price": 50.2,
        "tax": 10.5,
    },
}


@app.get(
    "/items/{item_id}/name",
    response_model=Item,
    response_model_include=["name", "description"],  # <- name , descri show hoga
)
async def read_item_name(item_id: str):
    return items[item_id]


@app.get("/items/{item_id}/public", response_model=Item, response_model_exclude=["tax"]) # # <- tax not show hoga
async def read_item_public_data(item_id: str):
    return items[item_id]


# --------------------------vid exam
# class Pakage(BaseModel):
#     name: str
#     no: str
#     description: Optional[str] = None
# @app.get('/check_respo1/')
# async def hello1():
#     return {'hello':'World'}
# @app.get('/pack/')
# async def make_pake(Package: Pakage):
#     return {"priority":priority,**package.dict(), "vale":vale}

