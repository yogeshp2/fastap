#Q_para_Ad
from typing import Union , List #<- for @app.get("/items8/") ->list

from fastapi import FastAPI , Query # <- for @app.get("/items1/")

from pydantic import Required #<- for @app.get("/items7/")


app = FastAPI()

                    # Query Parameters and String Validations
# 8000/items/?q=1
# 8000/items/ uper url and this line url will run
@app.get("/items/")
async def read_items(q: Union[str, None] = None): # <-here q can have str or none value defulat : NO requred give p values
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

                        #Additional validation
# we give optinal parapaer condition
# length doesn't exceed 50 characters.
# 8000/items1/?q=123456789 <- u cant enter extera 10th no in last line
# if we give value p or not its does't matter coz set none
# are same -> q: Union[str, None] = Query(default=None)
@app.get("/items1/")
async def read_items(q: Union[str, None] = Query(default=None, max_length=10)): # <- seted default value with the parameter Query(default=None)
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

                        #Add more validations
# 8000/items2/?q=555 <- it 5 , 55 or max length greater then 10 will give u error
@app.get("/items2/")
async def read_items(
    q: Union[str, None] = Query(default=None, min_length=3, max_length=10) #<- min length 3 vali given
):
    results = {"items": [{"item_id": "YOg"}, {"item_id": "PAr"}]}
    if q:
        results.update({"q": q})
    return results
                    #Add regular expressions
# 8000/items3/?q=yog -> q = xyz then show error ; if q =yog then it will show content
# regex-> for word ex: ^yog$ --> ^ : starting point & $ : End of string (regular expressions)
@app.get("/items3/")
async def read_items(
    q: Union[str, None] = Query(
        default=None, min_length=3, max_length=10, regex="^yog$"
    )
):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q ,'msg':'well u got right url!! well done  so this is msg'})
    return results
                    # Default values
# 8000/items4/ -> it will show q = yog value & means it will set defult value yog
# 8000/items4/?q=anythingelse --> it will sho p = anythingelse

@app.get("/items4/")
async def read_items(q: str = Query(default="yog", min_length=3)):
    results = {"items": [{"item_id": "honey"}, {"item_id": "singh"}]}
    if q:
        results.update({"q": q})
    return results

                    # Make it required
# just define which kind of data ex : q: str <- now u hav u give value compulsory
# u can also give query
# 8000/items5/?q=1 -> show somthing
# 8000/items5/ -> show nomthing error only
@app.get("/items5/")
async def read_items(q: str):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

                    # Required with Ellipsis (...) +       # Required with None
# ... -> its call as Ellipsis which say value is required / parameter is required.
# 8000/items6/?q=123 ---> run normaly
# 8000/items6/       ---> not run normaly ; error
@app.get("/items6/")
async def read_items(q: str = Query(default=..., min_length=3)): #<- here '...' show that parameter is required with min length 3
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

                    # Use Pydantic's Required instead of Ellipsis (...)
# Required == Ellipsis (...)
@app.get("/items7/")
async def read_items(q: str = Query(default=Required, min_length=3)):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

                    # Query parameter list / multiple values
# 8000/items8/?q=string1&q=string2&q=string3 --> string1 , string2 , string3 as input and o/p wil in list str1 to srt3
# 8000/items8/ it will give u null
@app.get("/items8/")
async def read_items(q: Union[List[str], None] = Query(default=None)):
    query_items = {"q": q}
    return query_items

                    # Query parameter list / multiple values with defaults
# 8000/items9/ --> it will show foo & bar
# 8000/items9/?q=foo&q=bar&q=string1&q=string2 --> it will show foo , bar , string1 , string2
@app.get("/items9/")
async def read_items(q: List[str] = Query(default=["foo", "bar"])):
    query_items = {"q": q}
    return query_items

                    # Declare more metadata
# title , description

@app.get("/items10/")
async def read_items(
    q: Union[str, None] = Query(
        default=None,
        title="Query string AAAAAAA", #<- title
        description="BBBBBB-description- Query string for the items to search in the database that have a good match", # <- description sshow in code
        min_length=3,
    )
):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

                     # Alias parameters
# 8000/items11/?item-query=dsds o/p: dsds  ,normaly python don;t allow this 'item-query'
@app.get("/items11/")
async def read_items(q: Union[str, None] = Query(default=None, alias="item-query")): #<- her item-query is 2nd name for q [alias use coz python dont support word-word str]
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

                # Deprecating parameters
# if u dont want  parameter anymore.
# n want to show in doc then use this
#  deprecated=True to Query
# 8000/items12/?item-query=yog --> run
# 8000/items12/ --> also run but not show q = yog
# 8000/items12/?q=1 --> also run but not show q = yoga
@app.get("/items12/")
async def read_items(
    q: Union[str, None] = Query(
        default=None,
        alias="item-query",
        title="Query string",
        description="Query string for the items to search in the database that have a good match",
        min_length=3,
        max_length=50,
        regex="^yog$",
        deprecated=True, #<-- show in doc- swaggerUI
    )
):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results

                    # Exclude from OpenAPI
# not show anyrhing in doc- swaggerUI
# simple give u that url access
@app.get("/items13/")
async def read_items(
    hidden_query: Union[str, None] = Query(default=None, include_in_schema=False)
):
    if hidden_query:
        return {"hidden_query": hidden_query}
    else:
        return {"hidden_query": "Not found"}