from typing import Union, List, Set  # <- for List @app.put("/items1/{item_id}") , Set <-@app.put("/items2/{item_id}")

from fastapi import FastAPI
from pydantic import BaseModel, HttpUrl #<- HttpUrl use for avalid url use in-@app.put("/items4/{item_id}")
app = FastAPI()


class Item(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: list = [] #<-- coz this we can enter list values in dictioary ,
    # definig list , just try in doc > Reuest body > addd multipale values
    # alaw all data types but if we want pertiqlar data type then see next code


@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item):
    results = {"item_id": item_id, "item": item}
    return results

# List(upper cap l) & list(small cap l) are diffent list-> simple python & List -> imported from pydantic

class Item1(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: List[str] = [] # <- its declear as a string
    number: List[int]


@app.put("/items1/{item_id}")
async def update_item(item_id: int, item: Item1):
    results = {"item_id": item_id, "item": item}
    return results

                        # Set types -> not allows duplicate values ,repeated value not allow
# ex : just we have to add--> tags: Set[str] = [] - tag become set value which not allow duplicate value
class Item2(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: Set[str] = set() #<-coz this it will give unique values only , uniqueItems: true


@app.put("/items2/{item_id}")
async def update_item(item_id: int, item: Item2):
    results = {"item_id": item_id, "item": item}
    return results

                    # Nested Models

class Image(BaseModel): #model-1
    url: str
    name: str


class Item3(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: set[str] = set()
    image: Union[Image, None] = None  #<- not madatry but if we enter this value then we have to enter url and name coz we declear here as a mandatary class Image(BaseModel) model-1:


@app.put("/items3/{item_id}")
async def update_item(item_id: int, item: Item3):
    results = {"item_id": item_id, "item": item}
    return results

                    # Special types and validation
#Note: if in url --> not mention http:// then wil give u error

class Image1(BaseModel):
    url: HttpUrl #<- coz this we can validate urls
    name: str


class Item4(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: set[str] = set()
    image: Union[Image1, None] = None


@app.put("/items4/{item_id}")
async def update_item(item_id: int, item: Item4):
    results = {"item_id": item_id, "item": item}
    return results

                    # Attributes with lists of submodels
# we also need something not only one image multiple images we neeed
class Image2(BaseModel):
    url: HttpUrl
    name: str

class Item5(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: set[str] = set()
    images: Union[list[Image2], None] = None


@app.put("/items5/{item_id}")
async def use_lists_submodels(item_id: int, item: Item5):
    results = {"item_id": item_id, "item": item}
    return results

                    # Deeply nested models

class Image3(BaseModel):
    url: HttpUrl
    name: str


class Item6(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: set[str] = set()
    images: Union[list[Image3], None] = None


class Offer(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    items: list[Item6]


@app.post("/offers/")
async def create_offer(offer: Offer):
    return offer

                    # Bodies of pure lists
# multiple url & name can send in form of list simple way
class Image4(BaseModel):
    url: HttpUrl
    name: str


@app.post("/images4/multiple/")
async def create_multiple_images(images: list[Image4]):
    return images

                    # Bodies of arbitrary dicts
# in normal dict value tpye alwas string type
# but in thish Dict we can define values and types also supose we want key as int and values shd have diff type(like int , str , float )
# Note: key in double quote-->  " ---- "
@app.post("/index-weights/")
async def create_index_weights(weights: dict[float, float]): #<-- here we declear dict key as integer and value as flot
    return weights






