# PathOperationConfiguration.py
from typing import Set, Union
from  enum import Enum
from fastapi import FastAPI, status
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: Set[str] = set()


@app.post("/items/", response_model=Item, status_code=status.HTTP_201_CREATED)
async def create_item(item: Item):
    return item


class Item1(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: Set[str] = set()


class Tags(Enum):
    items = "items"
    users = "users"
    tez = "testing"

@app.post("/items1/", response_model=Item1, tags=["items tags here"])
async def create_item(item: Item1):
    return item


@app.get("/items2/", tags=["items"])
async def read_items():
    return [{"name": "Foo", "price": 42}]


@app.get("/users2/", tags=["users"])
async def read_users():
    return [{"username": "johndoe"}]

@app.get("/items3/", tags=[Tags.items])
async def get_items():
    """
        Create an item with all the information:

        - **name**: each item must have a name
        - **description**: a long description
        - **price**: required
        - **tax**: if the item doesn't have tax, you can omit this
        - **tags**: a set of unique tag strings for this item
        """
    return ["Portal gun", "Plumbus"]


@app.get("/users4/",
         tags=[Tags.users],
        summary="Create an itemsf sdsadsad",
        description="Create an item with all the information, name, description, price, tax and a set of unique tags",
         )
async def read_users():
    return ["Rick", "Morty"]


class Item2(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None
    tags: Set[str] = set()


@app.post(
    "/items5/",
    response_model=Item2,
    summary="Create an item",
    response_description="The created item",
)
async def create_item(item: Item2):
    """
    Create an item with all the information:

    - **name**: each item must have a name
    - **description**: a long description
    - **price**: required
    - **tax**: if the item doesn't have tax, you can omit this
    - **tags**: a set of unique tag strings for this item
    """
    return item
@app.get("/elements/", tags=["items"], deprecated=True)  # blure
async def read_elements():
    return [{"item_id": "Foo"}]
