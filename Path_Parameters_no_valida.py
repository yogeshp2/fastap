from typing import Union

from fastapi import FastAPI, Path, Query #<-- Path importh for using path

app = FastAPI()


# 8000/items/1?item-query=Anything --> o/p "item_id": 1, q": "Anything" -- 1st val-->1 ?item-query= Anything <-- 2nd val
# path parameter is always required as it has to be part of the path.
@app.get("/items/{item_id}")
async def read_items(
    item_id: int = Path(title="The ID of the item to get"), # <- same query jese hum yha title declear kar skte hai Declare metadata , requed parameter
    q: Union[str, None] = Query(default=None, alias="item-query"),
):
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    return results

# # error code
# @app.get('/silly/{var}/{var1}')
# async def testing_som_error(var: int,var1 ):#<-- TypeError: unhashable type: 'set' coz 2nd var ko declear nhi kiya
#     return { {var} , {var1} }
# under this is solution --> change variable place 1st non set and set values

                    # Order the parameters as you need

# 8000/items1/123?q=wer o/p-> "item_id": 123, "q": "wer"
@app.get("/items1/{item_id}")
async def read_items(q: str, item_id: int = Path(title="The ID of the item to get")):
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    return results

                    # Order the parameters as you need, tricks
# * --> eyword arguments (key-value pairs), also known as kwargs. Even if they don't have a default value.
@app.get("/items2/{item_id}")
async def read_items(*, item_id: int = Path(title="The ID of the item to get" , default=None), q: str): #<-- * declear , q query parameter without a Query nor any default value,
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    return results

                    # Number validations: greater than or equal

# • gt: Greater than
# • ge: Greater than or equal to
# • lt: Less than
# • le: Less than or equal to
@app.get("/items3/{item_id}")
async def read_items(
    *, item_id: int = Path(title="The ID of the item to get", ge=1), q: str # <-- ge means greater then or equal to
):
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    return results

                    # Number validations: greater than and less than or equal
# 8000/items4/10?q=some%20string --> 10 ? ke place mai 11 then error
@app.get("/items4/{item_id}")
async def read_items(
    *,
    item_id: int = Path(title="The ID of the item to get", gt=0, le=10), #<-- greter then 0 and less then 1000
    q: str,
):
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    return results

                # Number validations: floats, greater than and less than
# 8000/float_test/10.5?q=str%20something&size=10.5
# o/p "item_id": 10.5, "q": "str something"
@app.get("/float_test/{item_id}")
async def read_items(
    *,
    item_id: float = Path(title="The ID of the item to get", ge=0, le=11.5), # <-- in float number compare
    q: str,
    size: float = Query(gt=0, lt=11.5)
):
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    return results

#ex: 2nd
@app.get("/testing_floating_nu/{val}")
async def test(val:int = Path(gt=0,le=12.5)):
    return({val})



