from typing import Union

from fastapi import FastAPI, Path , Body
from pydantic import BaseModel
app = FastAPI()


class Item(BaseModel):
    name: str                                 #<-- u have u name comparsaly
    description: Union[str, None] = None      #<-- optional make by None
    price: float                              #<-- u have u price comparsaly
    tax: Union[float, None] = None             #<-- optional make by None


@app.put("/items/{item_id}")
async def update_item(
    *,
    item_id: int = Path(title="title part here :The ID of the item to get", ge=0, le=1000),
    q: Union[str, None] = None,
    item: Union[Item, None] = None,
):
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    if item:
        results.update({"item": item})
    return results
#8000/items/123?q=hello%20there
# Output :
# {
#   "item_id": 123, #<-- from url
#   "q": "hello there",#<-- from url
#   "item": {
#     "name": "Yog",
#     "description": "NOthing :P ",
#     "price": 1000,
#     "tax": 80
#   }
# put method also give u input data + query values

# @app.get('/test/')
# async def test(
#
#         id: Path(default=None,ge=0,le=10) ,
#         id1:int
# ):
#     return {'id':id ,'id1':id1}

                            # Multiple body parameters
#8000/items1/420 + input 2-data ->item & user
class Item1(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None


class User(BaseModel):
    username: str
    full_name: Union[str, None] = None


@app.put("/items1/{item_id}")
async def update_item(item_id: int, item: Item1, user: User):
    results = {"item_id": item_id, "item": item, "user": user}
    return results

                    # Singular values in body
# 8000/items2/123
@app.put("/items2/{item_id}")
async def update_item(item_id: int, item: Item1, user: User, importance: int = Body() ):  #<- here we addd extra key importance name & u can add as u want
    results = {"item_id": item_id, "item": item, "user": user, "importance": importance}
    return results

                # Multiple body params and query

@app.put("/items3/{item_id}")
async def update_item(
    *,
    item_id: int,
    item: Item1,
    user: User,
    importance: int = Body(gt=0), #<- u can also decreal body part and declear part & see in redoc
    q: Union[str, None] = None #<-adding QUERY PARAMETERS , u dnt hav to explicitly add a Query
):
    results = {"item_id": item_id, "item": item, "user": user, "importance": importance}
    if q:
        results.update({"q": q})
    return results

                    # Embed a single body parameter

@app.put("/items4/{item_id}")
async def update_item(item_id: int, item: Item = Body(embed=True)): #<-- key value mai aayega & it will shown in Request body
    results = {"item_id": item_id, "item": item}
    return results

