from typing import Union

from fastapi import Body, FastAPI
from pydantic import BaseModel, Field

app = FastAPI()


class Item(BaseModel):
    name: str
    description: Union[str, None] = Field(  #<-- field
        default=None, title="The description of the item", max_length=300 #<-- Field added title will show in schema(doc)
    )
    price: float = Field(gt=0, description="The price must be greater than zero")  #<- Field added  description will show in schema(doc)
    tax: Union[float, None] = None


@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item = Body(embed=True)):
    results = {"item_id": item_id, "item": item}
    return results
# it will reflet on schema (see in bottum of doc) , item name will added