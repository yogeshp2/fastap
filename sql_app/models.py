from sqlalchemy import Boolean, Column, ForeignKey, Integer, String  # Create SQLAlchemy models for the Base class
from sqlalchemy.orm import relationship                              # Create the relationships

from .database import Base  # Import Base from database [file database.py] for Base inherit


class User(Base):               # Base inherit frm database.py
    __tablename__ = "users"     # attribute tells SQLAlchemy the name of the table

    id = Column(Integer, primary_key=True, index=True)      #  model (class) attributes
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    items = relationship("Item", back_populates="owner")    # Create the relationships


class Item(Base):               # Base inherit frm database.py
    __tablename__ = "items"     # attribute tells SQLAlchemy the name of the table

    id = Column(Integer, primary_key=True, index=True)      #  model (class) attributes
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))      # ForeignKey to Users's id

    owner = relationship("User", back_populates="items")    # Create the relationships
