from typing import List, Union

from pydantic import BaseModel


class ItemBase(BaseModel):          # initial Pydantic models / schemas
    title: str
    description: Union[str, None] = None


class ItemCreate(ItemBase):         # initial Pydantic models / schemas
    pass


class Item(ItemBase):               # models / schemas for reading / returning
    id: int
    owner_id: int

    class Config:       #  unique id is id and owner_id is FK , coz this we can use  id = data.id  instent of id = data["id"] ,maybe declear coz last inherited
        orm_mode = True


class UserBase(BaseModel):          # initial Pydantic models / schemas
    email: str


class UserCreate(UserBase):         # initial Pydantic models / schemas
    password: str


class User(UserBase):               # models / schemas for reading / returning
    id: int
    is_active: bool
    items: List[Item] = []

    class Config:       # id is unique id
        orm_mode = True

# Note: SQLAlchemy models define attributes using '='