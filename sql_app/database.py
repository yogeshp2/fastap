from sqlalchemy import create_engine                            # Import the SQLAlchemy parts
from sqlalchemy.ext.declarative import declarative_base         # Import the SQLAlchemy parts
from sqlalchemy.orm import sessionmaker                         # Import the SQLAlchemy parts

SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"  # Create a database URL for SQLAlchemy
# SQLALCHEMY_DATABASE_URL = "postgresql://user:password@postgresserver/db"

engine = create_engine(                                                     # Create the SQLAlchemy engine
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)     # Create a SessionLocal class , SessionLocal is a class

# bind – a Engine or other Connectable with which newly created Session objects will be associated.
# autoflush – The autoflush setting to use with newly created Session objects.
# autocommit – The autocommit setting to use with newly created Session objects.


Base = declarative_base()           # Create a Base class  , factory function , used to create base class.
# A base class stores a catlog of classes and mapped tables in the Declarative system , called as the declarative base class.
