from typing import Union, Optional
from fastapi import FastAPI, Body
from pydantic import BaseModel # <- use for request body


class Item(BaseModel): #<--model
    name: str
    description: Union[str, None] = None
    price: float
    tax: Union[float, None] = None


app = FastAPI()


@app.post("/items/")
async def create_item(item: Item):
    return item #<- using bloc

#--------------------------------
@app.post('/blog')
def creat_blog():
    return {'data':'user data sending to server'}

@app.post('/blog1')
def with_data_body(limit =10 , publiched:bool = True , sort: Optional[str]= None):
    if publiched:
        return {'data': f'--> {limit} <--- published blog from the db'}
    else:
        return {'data':f'--> {limit} <--- blog from the db'}

@app.post("/users2")
async def create_user(name: str = Body(...), age: int = Body(...)):
    return {"name": name, "age": age}

class User(BaseModel): # model 2
    name: str
    age: int
class Company(BaseModel): # model 1
    name: str
    address: Optional[str]=None # user agr ye data send bhi na kare then chalega
    phone_no: int

@app.post("/users3")
async def create_user(user: User):
    return user

@app.post("/users4") #<- multiple model can display here model 1 and model 2 use
async def create_user(user: User, company: Company):
    return {"user": user, "company": company}

# we can use condtion here for req body
@app.post("/users5")
async def create_user(user: User, priority: int = Body(...,ge=1, le=3)): # <-- 3< priority <1 if u will give out of range then it will give u error
    return {"user": user, "priority": priority}

# accessing model data by .value
@app.post("/items1/")
async def create_item(item: Item):
    item_dict = item.dict()
    if item.tax:
        price_with_tax = item.price + item.tax
        item_dict.update({"price_with_tax": price_with_tax})
    return item_dict