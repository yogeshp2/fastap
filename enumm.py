from enum import Enum

from fastapi import FastAPI,Path


class ModelName(str, Enum):
    a = "alexnet"
    resnet = "resnet"
    lenet = "lenet"


app = FastAPI()


@app.get("/models/{model_name}")
async def get_model(model_name: ModelName):
    if model_name == ModelName.a:
        return {"model_name": model_name, "message": "Deep Learning FTW!"}

    if model_name.value == "lenet":
        return {"model_name": model_name, "message": "LeCNN all the images"}

    return {"model_name": model_name, "message": "Have some residuals"}

#Path convertor # defin in --> 1st from fastapi import FastAPI,Path
@app.get("/files/{file_path:path}")
async def read_file(file_path: str):
    return {"file_path": file_path}

 #---> http://127.0.0.1:8000/users1/in here  1,2,3,4,5 values are not valid 7 or 8 or 10 or greter then 5
@app.get("/users1/{id}")
async def get_user(id: int = Path(..., le=5)):
    return {"id": id}


@app.get("/license-plates/{license}")
async def get_license_plate(license: str = Path(..., min_length=9, max_length=9)):
    return {"license": license}
#--->http://127.0.0.1:8000/license-plates/123456789 <-- min max val space mustbe 9 not less then 123456 or 12345678 ...

@app.get("/license-plates1/{license}" , tags=["license RRRR"] )
async def get_license_plate(license: str = Path(..., regex=r'^\w{2}-\d{3}-\w{2}$')):
    return {"license": license}
#--> advance of uper code -follow format ex: r"^\w{2}-\d{3}-\w{2}$" 1st-2 shd words (2), 2nd-digit , 3rd-word(3)
# tags=["license RRRR"] line reflets on docs& redoc like it will give u title in it docs
#--> i/p : http://127.0.0.1:8000/license-plates1/yo-123-yo  format:2words-3num-2words

