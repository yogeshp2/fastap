from fastapi import FastAPI, Query
from typing import Optional
from typing import Union
app = FastAPI()

fake_items_db = [{"item_name": "Foo"}, {"item_name": "Bar"}, {"item_name": "Baz"}]

# in normal usrl /items?limit=10published=true but we use skip&limit here we use this--> /items/?skip=0&limit=10
@app.get("/items/")
async def read_item(skip: int = 0, limit: int = 10):
    return fake_items_db[skip : skip + limit]


# use in url http://127.0.0.1:8000/users?page=2&size=10 2 to 10 data will display
# tag ='we force the page to be greater than 0 and the size to be less than or equal to 100'):
@app.get("/users")
async def get_user(page: int = Query(1, gt=0), size: int =Query(10, le=100)):
    return {"page": page, "size": size}

# 8000/blog?limit=123<-(last no part)limit we can fix user define
@app.get("/blog")
def index(limit):
    # return limit
    return {'data':f'{limit} no of blog from db'}

# 8000/blog1?limit=3&published=true -> published= true or false
# if u remove limit or published part (any one ) then its show error
# so for this problem we can set defult value see next code -- "/blog2"
# 8000/blog2?published=true here lime not define
# and aslo sort will also disable
@app.get("/blog1")
def index(limit , published: bool):
    if published:
        return {'data':f'{limit} publishef blog from db'}
    else:
        return {'data': f'{limit} blog from db'}

# we can set defult velue & also set Optional parameters + from typing import option
@app.get("/blog2")
def index(limit=10 , published: bool = True, sort: Optional[str] = None):
    if published:
        return {'data':f'{limit} publishef blog from db'}
    else:
        return {'data': f'{limit} blog from db'}

                        # Multiple path and query parameters
# 8000/users1/123/items/12?q=Yogesh&short=false here q= Yogesh
@app.get("/users1/{user_id}/items/{item_id}")
async def read_user_item(
    user_id: int, item_id: str, q: Union[str, None] = None, short: bool = False
):
    item = {"item_id": item_id, "owner_id": user_id}
    if q:
        item.update({"q": q})
    if not short:
        item.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return item

                            #Required query parameters
# But when you want to make a query parameter required, you can just not declare any default
# 8000/items1/123?needy=Yogesh
# 8000/items/foo-item <-- make error
# item_id: str, needy: str <--- this will make required parameter.
# also we can define defult values also
@app.get("/items1/{item_id}")
async def read_user_item(item_id: str, needy: str):
    item = {"item_id": item_id, "needy": needy}
    return item
# also we can define defult values also uper code update
@app.get("/items2/{item_id}")
async def read_user_item(item_id: str, needy: str, sort: Union[str,None]=None):
    item = {"item_id": item_id, "needy": needy}
    return item


