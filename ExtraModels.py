from typing import Union, List

from fastapi import FastAPI
from pydantic import BaseModel, EmailStr

app = FastAPI()


class UserIn(BaseModel):
    username: str
    password: str
    email: EmailStr
    full_name: Union[str, None] = None


class UserOut(BaseModel):
    username: str
    email: EmailStr
    full_name: Union[str, None] = None


class UserInDB(BaseModel):
    username: str
    hashed_password: str
    email: EmailStr
    full_name: Union[str, None] = None


def fake_password_hasher(raw_password: str):
    return "supersecret" + raw_password


def fake_save_user(user_in: UserIn):
    hashed_password = fake_password_hasher(user_in.password)
    user_in_db = UserInDB(**user_in.dict(), hashed_password=hashed_password)
    print("User saved! ..not really")
    return user_in_db


@app.post("/user/", response_model=UserOut)
async def create_user(user_in: UserIn):
    user_saved = fake_save_user(user_in)
    return user_saved

# user_in = UserIn(username="john", password="secret", email="john.doe@example.com")
# user_dict = user_in.dict()
# print(user_dict)
#

# from pydantic import BaseModel
#
# class Blog(BaseModel):
#     title: str
#     is_active: bool
#
# Blog(title="My First Blog",is_active=True)

# import time
# from pydantic import BaseModel
# from datetime import datetime
#
# class Blog(BaseModel):
#     title: str
#     created_at: datetime = datetime.now()
#     is_active: bool
#
# print(Blog(title="Our First Blog",is_active=True))
# time.sleep(5)
# print(Blog(title="Our Second Blog",is_active=True))
class Item(BaseModel):
    name: str
    description: Union[str, None] = None
    price: float
    tax: float = 10.5
    tags: List[str] = []


items = {
    "foo": {"name": "Foo", "price": 50.2},
    "bar": {"name": "Bar", "description": "The bartenders", "price": 62, "tax": 20.2},
    "baz": {"name": "Baz", "description": None, "price": 50.2, "tax": 10.5, "tags": []},
}


@app.get("/items/{item_id}", response_model=Item, response_model_exclude_unset=True)
async def read_item(item_id: str):
    return items[item_id]
