# BodyUpdates
from typing import List, Union

from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    name: Union[str, None] = None
    description: Union[str, None] = None
    price: Union[float, None] = None
    tax: float = 10.5
    tags: List[str] = []


items = {
    "foo": {"name": "Foo", "price": 50.2},
    "bar": {"name": "Bar", "description": "The bartenders", "price": 62, "tax": 20.2},
    "baz": {"name": "Baz", "description": None, "price": 50.2, "tax": 10.5, "tags": []},
}


@app.get("/items/{item_id}", response_model=Item)
async def read_item(item_id: str):
    return items[item_id]


@app.put("/items1/{item_id}", response_model=Item)
async def update_item(item_id: str, item: Item):
    update_item_encoded = jsonable_encoder(item)
    items[item_id] = update_item_encoded
    return update_item_encoded
# --                Partial updates with PATCH

class Item1(BaseModel):
    name: Union[str, None] = None
    description: Union[str, None] = None
    price: Union[float, None] = None
    tax: float = 10.5
    tags: List[str] = []


items1 = {
    "foo": {"name": "Foo", "price": 50.2},
    "bar": {"name": "Bar", "description": "The bartenders", "price": 62, "tax": 20.2},
    "baz": {"name": "Baz", "description": None, "price": 50.2, "tax": 10.5, "tags": []},
}


@app.get("/items2/{item_id}", response_model=Item1)
async def read_item(item_id: str):
    return items1[item_id]


@app.patch("/items3/{item_id}", response_model=Item1)
async def update_item(item_id: str, item: Item1):
    stored_item_data = items1[item_id]
    stored_item_model = Item1(**stored_item_data)
    update_data = item.dict(exclude_unset=True) # exclude_unset ko true for particular update
    updated_item = stored_item_model.copy(update=update_data)
    items1[item_id] = jsonable_encoder(updated_item)
    return updated_item

                    # Using Pydantic's update parameter
