from fastapi import FastAPI, File, UploadFile
from typing import Union, List
from fastapi.responses import HTMLResponse

app = FastAPI()

# File is a class that inherits directly from Form.
@app.post("/files/")
async def create_file(file: bytes = File()):
    return {"file_size": len(file)}  #<-- file size o/p


@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile):
    return {"filename": file.filename}

                    # File Parameters with UploadFile

@app.post("/files1/")
async def create_file(file: Union[bytes, None] = File(default=None)):
    if not file:
        return {"message": "No file sent"}
    else:
        return {"file_size": len(file)}


@app.post("/uploadfile1/")
async def create_upload_file(file: Union[UploadFile, None] = None):
    if not file:
        return {"message": "No upload file sent"}
    else:
        return {"filename": file.filename}

                    # Optional File Upload
@app.post("/files2/")
async def create_file(file: Union[bytes, None] = File(default=None)): #<- decleared optiona
    if not file:
        return {"message": "No file sent"}
    else:
        return {"file_size": len(file)}


@app.post("/uploadfile2/")
async def create_upload_file(file: Union[UploadFile, None] = None): #<- decleared optiona in UploadFile method
    if not file:
        return {"message": "No upload file sent"}
    else:
        return {"filename": file.filename }

# @app.post("/upload")
# async def upload_file(file: UploadFile = File(...)):
#     with open(file.filename, 'wb') as image:
#         content = await file.read()
#         image.write(content)
#         image.close()
#     return JSONResponse(content={"filename": file.filename},
# status_code=200)

                    # UploadFile with Additional Metadata
@app.post("/files3/")
async def create_file(file: bytes = File(description="description define here & A file read as bytes")):
    return {"file_size": len(file)}


@app.post("/uploadfile3/")
async def create_upload_file(
    file: UploadFile = File(description="description define here & A file read as UploadFile"),
):
    return {"filename": file.filename}

                    # Multiple File Uploads
@app.post("/files4/")
async def create_files(files: List[bytes] = File()):
    return {"file_sizes": [len(file) for file in files]}


@app.post("/uploadfiles4/")
async def create_upload_files(files: List[UploadFile]):
    return {"filenames": [file.filename for file in files]}


@app.get("/htmlcode")
async def main():
    content = """
<body>
<form action="/files/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
<form action="/uploadfiles/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
</body>
    """
    return HTMLResponse(content=content)

@app.post("/files5/")
async def create_files(
    files: List[bytes] = File(description="Multiple files as bytes"),
):
    return {"file_sizes": [len(file) for file in files]}


@app.post("/uploadfiles5/")
async def create_upload_files(
    files: List[UploadFile] = File(description="here description written , Multiple files as UploadFile"),
):
    return {"filenames": [file.filename for file in files]}


@app.get("/htmlcode1")
async def main():
    content = """
<body>
<form action="/files/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
<form action="/uploadfiles/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
</body>
    """
    return HTMLResponse(content=content)
