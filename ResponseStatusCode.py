# ResponseStatusCode for ex: 404 page not found
from fastapi import FastAPI, status, Response

app = FastAPI()


@app.post("/items/", status_code=201) # status code 201 is creared , it declrea in path
async def create_item(name: str):
    return {"this is status code": name}


@app.post("/items1/", status_code=status.HTTP_404_NOT_FOUND) # in FastAPI short cut given just use status.name of response
async def create_item(name: str):
    return {"this is status code": name}

# Change / modified Status Code


tasks = {"foo": "Listen to the Bar Fighters"}


@app.put("/get-or-create-task/{task_id}", status_code=200)
def get_or_create_task(task_id: str, response: Response):
    if task_id not in tasks:
        tasks[task_id] = "This didn't exist before"
        response.status_code = status.HTTP_201_CREATED
    return tasks[task_id]

