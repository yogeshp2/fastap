from typing import Union , List

from fastapi import Cookie, FastAPI, Header #<-- cookie, Header import here

app = FastAPI()


@app.get("/cok/")
async def read_items(ads_id: Union[str, None] = Cookie(default=None)): #<- cookie declear here --> or Cookie(None)
    return {"ads_id": ads_id}

@app.get("/cok-testing/")
async def read_items(ads_id: Union[str, None] = Cookie(None)): #<- cookie declear here --> or Cookie(None)
    return {"ads_id": ads_id}
# HTTP headers are case-insensitive,
@app.get("/head/")
async def read_items(user_agent: Union[str, None] = Header(default=None)): #< if u dont give value of user_agen then no error
    return {"User-Agent": user_agent}
# uper code and belov code are same [head and head3] just we have to give value
@app.get("/head3/")
async def get_header(user_agent: str = Header(...)):#< if u dont give value of user_agen then error
    return {"user_agent": user_agent}

@app.get("/head2/")
async def get_header(hello: str = Header(...)):
    return {"hello": hello}

@app.get("/head3/")
async def read_items(
    strange_header: Union[str, None] = Header(default=None, convert_underscores=False) #<-- convert_underscores to False auto (-) will disable
):
    return {"strange_header": strange_header}

                    # Duplicate headers
# some time we receive Multiple header --> same header -->multiple values so we use List
@app.get("/items/")
async def read_items(x_token: Union[List[str], None] = Header(default=None)):
    return {"X-Token values": x_token}

@app.get('/needCookie')
def need_cookie_route(test: Union[str, None] = Cookie(...)):
    return test